# Introduction #

For the past half decade or so, I have been downloading videos from kink.com and storing them locally on my own media server so that the SO and I can watch them on the TV. Originally, I was doing this manually, and then I started using a series of shell scripts to download them via curl. After maintaining that solution for a couple years, I decided to do a full rewrite in a more suitable language. "Kinkdownloader" is the fruit of that labor.

# Features #

* Allows archiving of individual shoots or full channel galleries.
* Downloads highest quality shoot videos with user-selected cutoff.
* Creates Emby/Kodi compatible NFO files containing: 
  * Shoot title 
  * Shoot date
  * Scene description 
  * Genre tags 
  * Performer information
* Downloads performer bio images.
* Downloads shoot thumbnails.
* Downloads shoot "poster" image.

# Screenshot #

![Running](resources/running.png "Screenshot of script running.")

# Requirements #

* [Python 3.7+](https://www.python.org/)
* [BeautifulSoup4](https://www.crummy.com/software/BeautifulSoup/)
* [cloudscraper](https://github.com/VeNoMouS/cloudscraper)
* [tqdm](https://tqdm.github.io/)

Kinkdownloader also requires a Netscape "cookies.txt" file containing your kink.com session cookie. You can create one manually, or use a browser extension like ["cookies.txt"](https://addons.mozilla.org/en-US/firefox/addon/cookies-txt/). Its default location is ~/cookies.txt [or Windows/MacOS equivalent]. This can be changed with the --cookies flag.

# Usage #

## General ##
This script will download the given shoot including all accompanying metadata and attached media.

You can get per shoot:
- the video
- the poster
- the attached image gallery
- the nfo-file for Emby/Kodi with all relevant metadata

Additionally this script will also get (per shoot)
- the models/performers in the shoot with their poster picture
- they are put in a folder/subfolder structure like `A/Abella Danger` or `V/Veronica Avluv`

If no quality is defined it will always try to download 1080p (Full HD quality).

The available qualities are: 1080, 720, 540, 480, 360, 288, 270

## Prepare a download folder ##
If you use the script, be aware that it will download everything into the same folder you are calling it from.

To seperate the script and the content, create a download folder and then call the script from inside of it. You will also need to use the `-c PATHTOCOOKIES/cookies.txt` parameter if saved outside of the download folder.

## Multiple shoots/channels/galleries/search results ##
This script can automate download of full channels or other multiple shoots. Please see the examples below.

You need to be aware that excessive download will result in a temporary ban from kink.com, so you can't download more files for a period.

This script checks automatically if you already have download a file and skips the download if so. This means you can start the same command again and it will download only the missing parts.

## Screenshot ##
**This screenshot is outdated**

![Usage](resources/usage.png "Usage example.")



# FAQ #

## Examples? ##

Want to download just the video for a single shoot?

    kinkdownloader --no-metadata https://www.kink.com/shoot/XXXXXX

Want to download only the metadata?

    kinkdownloader --no-video https://www.kink.com/shoot/XXXXXX

How about downloading the latest videos from your favorite channel?

    kinkdownloader https://www.kink.com/channel/CHANNELXXX/latest/page/1

Want to archive a full channel?

    kinkdownloader -r https://www.kink.com/channel/CHANNELXXX/latest/page/1

Want to download all videos for a model or any other search request?

Hint 1: Pay attention to the quotation marks!

Hint 2: You can get the link by copying the first page link on the bottom (where you navigate to page 1, page 2, page 3 and so on)

    kinkdownloader -r "https://www.kink.com/search?type=shoots&performerIds=XXXX&sort=published&page=1"

Want to download the video in a specific quality?

    kinkdownloader -q 720 https://www.kink.com/shoot/XXXXXX

## Where do I get it? ##

There is a git repository located [here](https://gitlab.com/meanmrmustardgas/kinkdownloader).

## How can I report bugs/request features? ##

You can either PM me on [reddit](https://www.reddit.com/u/mean_mr_mustard_gas), post on the [issues board](https://gitlab.com/meanmrmustardgas/kinkdownloader/-/issues) on gitlab, or send an email to meanmrmustardgas at protonmail dot com.

## This is awesome. Can I buy you beer/hookers? ##

Sure. If you want to make donations, you can do so via the following crypto addresses:

![XMR](resources/Monero_QR.png "468kYQ3vUhsaCa8zAjYs2CRRjiqNqzzCZNF6Rda25Qcz2L8g8xZRMUHPWLUcC3wbgi4s7VyHGrSSMUcZxWQc6LiHCGTxXLA")
![BTC](resources/Bitcoin_QR.png "3CcNQ6iA1gKgw65EvrdcPMe12Heg7JRzTr")
![ETH](resources/Ethereum_QR.png "0xa685951101a9d51f1181810d52946097931032b5")
![XLM](resources/Stellar_QR.png "GDZOWSAH4GTZPZEK6HY3SW2HLHOH6NAEGHLEIUTLT46C6V7YJGEIJHGE")
![DOGE](resources/Dogecoin_QR.png "DKzojbE2Z8CS4dS5YPLHagZB3P8wjASZB3")
![LTC](resources/Litecoin_QR.png "MFcL7C2LzcVQXzX5LHLVkycnZYMFcvYhkU")

# TODO #

* Rate limiting to avoid temporary bans.
